"""authProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from TeacompanoApp import views

urlpatterns = [
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
    path('user/update/<int:pk>/', views.UserUpdateView.as_view()),
    path('user/delete/<int:pk>/', views.UserDestroyView.as_view()),

    path('service/', views.ServiceCreateView.as_view()),
    path('service/<int:pk>/', views.ServiceDetailView.as_view()),
    path('service/update/<int:pk>/', views.ServiceUpdateView.as_view()),
    path('service/delete/<int:pk>/', views.ServiceDestroyView.as_view()),

    path('professional/', views.ProfessionalCreateView.as_view()),
    path('professional/<int:pk>/', views.ProfessionalDetailView.as_view()),
    path('professional/update/<int:pk>/', views.ProfessionalUpdateView.as_view()),
    path('professional/delete/<int:pk>/', views.ProfessionalDestroyView.as_view()),
]