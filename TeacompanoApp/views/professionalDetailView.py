from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated

from TeacompanoApp.models.professional import Professional
from TeacompanoApp.serializers.professionalSerializer import ProfessionalSerializer

class ProfessionalDetailView(generics.RetrieveAPIView):
    queryset = Professional.objects.all()
    serializer_class = ProfessionalSerializer
    
    def get(self, request, *args, **kwargs):
        
        return super().get(request, *args, **kwargs)