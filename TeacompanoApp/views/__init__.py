from .userCreateView import UserCreateView
from .userDetailView import UserDetailView
from .userDestroyView import UserDestroyView
from .userUpdateView import UserUpdateView

from .serviceCreateView import ServiceCreateView
from .serviceDetailView import ServiceDetailView
from .serviceDestroyView import ServiceDestroyView
from .serviceUpdateView import ServiceUpdateView

from .professionalCreateView import ProfessionalCreateView
from .professionalDetailView import ProfessionalDetailView
from .professionalDestroyView import ProfessionalDestroyView
from .ProfessionalUpdateView import ProfessionalUpdateView

