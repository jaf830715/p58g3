from rest_framework import status, views
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from TeacompanoApp.serializers.professionalSerializer import ProfessionalSerializer

class ProfessionalCreateView(views.APIView):

    def post(self, request, *args, **kwargs):
        serializer = ProfessionalSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
         
        return Response({'Profesional creado correctamente'},status=status.HTTP_201_CREATED)