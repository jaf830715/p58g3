from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response


from TeacompanoApp.models.professional import Professional
from TeacompanoApp.serializers.professionalSerializer import ProfessionalSerializer

class ProfessionalUpdateView(generics.RetrieveUpdateAPIView):
    queryset = Professional.objects.all()
    serializer_class = ProfessionalSerializer
   
    def put(self, request, *args, **kwargs):
    
        super().put(request, *args, **kwargs)

        return Response({'Profesional Actualizado correctamente'},status=status.HTTP_200_OK)