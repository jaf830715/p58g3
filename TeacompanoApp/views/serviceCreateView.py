from rest_framework import status, views
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from TeacompanoApp.serializers.serviceSerializer import ServiceSerializer

class ServiceCreateView(views.APIView):

    def post(self, request, *args, **kwargs):
        serializer = ServiceSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        tokenData = {"iduser":request.data["iduser"], 
                     "password":request.data["password"]}
        tokenSerializer = TokenObtainPairSerializer(data=tokenData)
        tokenSerializer.is_valid(raise_exception=True)
                
        return Response({'Servicio creado correctamente'},status=status.HTTP_201_CREATED)