from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password

class UserManager(BaseUserManager):
    def create_user(self,iduser, password=None):
        """
        Creates and saves a user with the given iduser and password.
        """
        if not iduser:
            raise ValueError('Users must have an iduser')
        user = self.model(iduser=iduser)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, iduser, password):
        """
        Creates and saves a superuser with the given iduser and password.
        """
        user = self.create_user(
            iduser=iduser,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser, PermissionsMixin):
    id= models.BigAutoField(primary_key=True)
    iduser = models.IntegerField('Número de documento', unique=True)
    password = models.CharField('Contraseña', max_length = 256)
    tipoid = models.CharField('Tipo de documento',max_length=2)
    name = models.CharField('Nombre Paciente', max_length = 30)
    surname = models.CharField('Apellido Paciente', max_length = 30)
    name2 = models.CharField('Nombre Responsable', max_length = 30, null=True, blank=True)
    surname2 = models.CharField('Apellido Responsable', max_length = 30, null=True, blank=True)
    email = models.EmailField('Email', max_length = 100)
    mobile = models.BigIntegerField('Número Contacto')
    address = models.CharField('Dirección', max_length = 30)

    def save(self, **kwargs):
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN' 
        self.password = make_password(self.password, some_salt)
        super().save(**kwargs)

    objects = UserManager()
    USERNAME_FIELD = 'iduser'
