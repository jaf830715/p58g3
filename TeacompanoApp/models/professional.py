from django.db import models
from .user import User

class Professional(models.Model):
    idprofessional = models.AutoField(primary_key=True)
    name = models.CharField('Nombre profesional', max_length = 50)
    profession = models.CharField('profesional', max_length = 400)
    gender = models.CharField('Genero', max_length = 10)
    availability = models.CharField('Disponibilidad', max_length = 30)