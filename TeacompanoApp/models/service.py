from django.db import models

from TeacompanoApp.models.professional import Professional
from .user import User

class Service(models.Model):
    codservice = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='service', on_delete=models.CASCADE)
    professional = models.ForeignKey(Professional, related_name='professional', on_delete=models.CASCADE)
    date = models.DateField('Fecha')
    place = models.CharField('Lugar', max_length = 100)
    duration = models.CharField('Tiempo servicio', max_length = 30)
    score = models.IntegerField('Calificación')
    observation = models.TextField('Observaciónes', max_length = 400)
    