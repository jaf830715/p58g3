from TeacompanoApp.models.professional import Professional
from rest_framework import serializers

class ProfessionalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Professional
        fields = ['idprofessional', 'name', 'profession','gender','availability']