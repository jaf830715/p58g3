from rest_framework import serializers
from TeacompanoApp.models.user import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['iduser', 'tipoid','password','name','surname','name2','surname2','email','mobile','address']

    def create(self, validated_data):
        userInstance = User.objects.create(**validated_data)
        return userInstance

    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)   
        return {
                    'id':user.id,          
                    'iduser': user.iduser, 
                    'tipoid': user.tipoid,
                    'name': user.name,
                    'surname': user.surname,
                    'name2': user.name2,
                    'surname2': user.surname2,
                    'email': user.email,
                    'mobile': user.mobile,
                    'address': user.address,
                }