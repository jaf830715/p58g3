from TeacompanoApp.models.service import Service
from rest_framework import serializers

class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = ['codservice', 'user', 'professional','date','place','duration','score','observation']
