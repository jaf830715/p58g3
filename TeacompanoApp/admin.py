from django.contrib import admin
from .models.user import User
from .models.service import Service
from .models.professional import Professional

admin.site.register(User)
admin.site.register(Service)
admin.site.register(Professional)